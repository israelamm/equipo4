import {html, LitElement, css} from 'lit-element'

    class AppHeader extends LitElement{
        static get styles(){
          return css 
          `
            nav{
              background-color: #072146;
            }

            li{
              height: 82.500px;
              margin: 0 15px;
            }

            li>a{
              line-height: 58px;
              color: grey !important;
              font-weight: bold;
              font-size: 13px;
              border-bottom:5px solid transparent;
              cursor: pointer;
            }

            li>a.active{
              line-height: 55px;
              color: white !important;
              border-bottom:5px solid white;
              cursor: pointer;
            }

            svg{
              color: #1973b8;
            }

            .navbar-brand{
              cursor:pointer;
            }

          `;
        }

        render() {
            return html 
                `
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

                <nav class="navbar navbar-expand-md p-0 px-5">
                <div class="container-fluid">
                  <a class="navbar-brand" @click="${this.goMain}">
                  <img src="https://www.bbva.mx/content/dam/public-web/global/images/logos/logo_bbva_blanco.svg" alt="img-bbva" width="120" height="70">
                  </a>
                  <button class="navbar-toggler bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                  <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                </svg>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                      <li class="nav-item">
                        <a id="aMain" class="nav-link active" aria-current="page" @click="${this.goMain}">CRÉDITO AUTOS</a>
                      </li>

                      <li class="nav-item">
                        <a id="aQuotes" class="nav-link" aria-current="page" @click="${this.goQuotes}">CONSULTAR COTIZACIONES</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
                `;
        }

        goMain() {
          this.menuEvent('main');
          this.shadowRoot.querySelector('#aMain').classList.add("active");
          this.shadowRoot.querySelector("#aQuotes").classList.remove("active");
        }

        goQuotes() {
          this.menuEvent('quotes');
          this.shadowRoot.querySelector('#aMain').classList.remove("active");
          this.shadowRoot.querySelector("#aQuotes").classList.add("active");
        }

        menuEvent(page) {
          let namePage = 'go-to-'+page;
          this.dispatchEvent( new CustomEvent(namePage, {} ));
        }
    }

customElements.define('app-header', AppHeader);