import { LitElement, html, css } from 'lit-element';

export class AppCotizadorGeneral extends LitElement {
    static get properties() {
      return {
        auto: { type: Object }
      };
    }
  
    static get styles() {
      return css`

      .card{
        border:none;
      }
      .card-footer{
        background-color: #f4f4f4;
        border: none;
       }

       .card-body{
        background-color: #eaf9fa;
       }

       .card-text{
           color: black;
           font-size: 15px;
           font-weight: bold;
       }

       .card-title{
           color: #121212;
           font-size: 26px !important;
           text-transform: uppercase;
           font-weight: bold;
           text-align: center;
       }

       button{
           color: white;
           font-size: 15px;

           font-weight: bold;

           outline:none;

           border:none;

           height: 50px;

           background-color: #1973b8;
       }

       .button:focus, .form-select:focus, .form-control:focus {
        outline: none;
        box-shadow: none;
      }


      select{
        background-color: white;
        border:none;
      }

      .form-control{
        height: 50px !important;
      }

      .placeh{
        font-size: 16px;
        color: #1973b8;
      }

      .div-img{
        width:100%;
      }

      img{
        object-fit: cover;
      }
      `;
    }
  
    constructor() {
      super();
      this.auto = {};
    }
  
    render() {
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <div class="card h-100">
        <button @click="${this.goBack}" class="d-inline-block col-5 my-3">Regresar</button>
          <div class="div-img">
          <img src="${this.auto.imagen}" width="100" height="350" class="card-img-top"/>
          </div>
          <div class="card-body pt-0">
              <div class="container mt-3">
                <div class="row d-flex align-items-end">
                <div class="col-12 col-lg-3">
                  <img src="https://www.bbva.mx/content/dam/public-web/global/images/micro-illustrations/official_document.svg" class="d-none d-lg-inline"  height="60" width="30" class="card-img-top"/>
                  </div>
                  <div class="col-12 col-lg-9">
                    <h5 class="card-title">${this.auto.marca} - ${this.auto.modelo}</h5>
                  </div>
                </div>
                <div class="row card-text mt-4">
                <div class="col-6 offset-3 col-lg-4 offset-lg-0">
                Año: ${this.auto.anno}
                </div>

                <div class="col-10 offset-1 col-lg-8 offset-lg-0 text-lg-end mt-2 mt-lg-0">
                Precio: $${this.auto.precio} MXN
                </div>
                </div>
                <div class="row mt-3">
                  <div class="col-12 g-0">
                  <div class="form-floating">
                    <input type="text" class="form-control" id="txtNombre" placeholder="Nombre">
                    <label class="placeh">Nombre</label>
                  </div>

                  <div class="form-floating my-1">
                    <input type="number" class="form-control" id="txtEnganche" placeholder="Nombre">
                    <label class="placeh">Enganche</label>
                  </div>

              <p class="card-text mt-3">Meses: 

                <select name="meses" class="form-select" aria-label="Default select example">
                  <option selected>Selecciona una opción</option>
                  <option value="12">12 meses</option>
                  <option value="24">24 meses</option>
                  <option value="36">36 meses</option>
                  <option value="48">48 meses</option>
                  <option value="60">60 meses</option>
                </select>
              </p>
                  <div>
                </div>
                
              </div>

              
          </div>
          <div class="card-footer container bg-transparent">
          <div class="row d-flex justify-content-end bg-transparent">
            <button @click="${this.calculate}" class="d-inline-block col-5">Calcular</button>
          </div>
            
          </div>
        </div> 
      `;
    }

    goBack(e) {
      console.log("goBack");	  
      e.preventDefault();	
      this.auto = {};
      this.dispatchEvent(new CustomEvent("go-to-select-car",{}));	
    }

    calculate() {
      let dataToCalculate = this.auto;
      dataToCalculate.cliente = this.shadowRoot.getElementById('txtNombre').value;
      dataToCalculate.enganche = this.shadowRoot.getElementById('txtEnganche').value;
      dataToCalculate.meses = this.shadowRoot.querySelector('[name="meses"]').value;
      console.log(dataToCalculate);
      this.dispatchEvent(
        new CustomEvent("data-to-calculate", {
                bubbles: true,
                detail: dataToCalculate
            }
        )
      );
    }

}

customElements.define('app-cotizador-general', AppCotizadorGeneral);


