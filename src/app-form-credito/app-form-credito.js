import { LitElement, html, css } from 'lit-element';

export class AppFormCredito extends LitElement {
    static get properties() {
      return {
        ranks: { type: Array }
      };
    }
  
    static get styles() {
      return css`
        label{
          color: #1973b8;
          font-size: 20px;
          font-weight: bold;
          margin-bottom:5px;
        }

        .form-select:focus{
          outline: none;
          box-shadow: none;
        }

        option{
          color: #626262;
           font-size: 15px;
           font-weight: bold;
        }
      `;
    }
  
    constructor() {
      super();
      this.ranks =[];
      this.getRanks();
    }
  
    render() {
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <div class="mx-2 my-3">
            <label>Rangos de Sueldo:</label>
            <select name="Rangos" @change="${this.rankChange}" class="form-select" aria-label="Default select example">
            <option selected><label class="seleccion">Selecciona una opción</label></option>
                ${this.ranks.map(
                    rank => 
                        html`<option value="${rank.rango}">${rank.min} - ${rank.max}</option>`
                    )}
            </select>
            <div class="mt-2 mt-md-5">
            <img src="https://www.bbva.mx/content/dam/public-web/mexico/photos/woman_mobile_auto-card.jpg.img.768.1620840348849.jpg" class="img-fluid" alt="...">
            </div>
        </div>
      `;
    }

   
   
   getRanks(){
    //let url = "https://cotizador-autos-springboot.herokuapp.com/apicotizador/v1/rangos";
    let url = "/data/rangos.json"
    let xhr = new XMLHttpRequest();
    xhr.onload = function (){
        if (xhr.status ===200){
            let APIResponse = JSON.parse(xhr.responseText);
            this.ranks = APIResponse ;
        }
    }.bind(this);
    xhr.open("GET",url);
    xhr.send();
    
  }

    rankChange(e) {
        this.dispatchEvent(new CustomEvent('rank-selected', {
            detail: e.target.value
          }));
    }

}

customElements.define('app-form-credito', AppFormCredito);


