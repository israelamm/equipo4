import { LitElement, html, css} from 'lit-element';

class AppAutoFicha extends LitElement {

    static get properties() {
        return {          
                id:{type:String},
                marca:{type:String},
                modelo:{type:String},
                anno:{type:Number},
                precio:{type:Number},
                imagen:{type:String},
                
        }
    }

    static get styles(){
        return css 
        `
           .card-footer{
            background-color: #f4f4f4;
            border: none;
           }

           .card-body{
            background-color: #f4f4f4;
           }

           .card-text{
               color: #626262;
               font-size: 15px;
               font-weight: lighter !important;
           }

           .card-title{
               color: #121212;
               font-size: 20px !important;
               text-transform: uppercase;
               font-weight: bold;
               height: 45px;
           }

           .btn{
               color: #1973b8;
               font-size: 15px;

               font-weight: bold;

               outline:none;
           }

           .btn:focus {
            outline: none;
            box-shadow: none;
          }

          svg{
              color: #1973b8;
              font-weight: bold;
          }

          img{
            object-fit: cover;
          }

        `;
    }

    constructor() {
        super();
        this.id="";
        this.marca="";
        this.modelo="";
        this.anno=0;
        this.precio=0;
        this.imagen="";
    }

    render() {
        return html`

        
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <div class="h-100 mx-3 mt-4 mt-md-0 mb-md-4">
            <div class="div-img">
            <img src="${this.imagen}" width="100" height="250" class="card-img-top"/>
            </div>
                
                <div class="card-body">
                    <h5 class="card-title">${this.marca} - ${this.modelo}</h5>
                    <p class="card-text"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash" viewBox="0 0 16 16">
                    <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                  </svg>Año: ${this.anno}</p>

                  <p class="card-text"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash" viewBox="0 0 16 16">
                  <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                </svg>Precio: ${this.precio}</p>
                </div>
                <div class="card-footer">
			        <button @click="${this.cotizarAuto}" class="btn col-12 bg-transparent"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                    <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                  </svg> Cotizar</button>
		        </div>	                
            </div>            
        `;
    }

    cotizarAuto(e) {
        console.log("Se va a cotizar el auto " + this.id); 
        
            this.dispatchEvent(
                new CustomEvent("cotizar-auto", {
                        bubbles: true,
                        detail: {
                            id:this.id,
                            marca:this.marca,
                            modelo:this.modelo,
                            anno:this.anno,
                            precio:this.precio,
                            imagen:this.imagen
                        }
                    }
                )
            );
        }
}

customElements.define('app-auto-ficha', AppAutoFicha)