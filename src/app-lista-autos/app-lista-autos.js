import { LitElement, html, css } from 'lit-element';
import '../app-auto-ficha/app-auto-ficha'


class AppListaAutos extends LitElement {

    static get properties() {
        return {
            autos: {type: Array},
            rango: {type:Number}
        };
    }

    static get styles(){
        return css 
        `
            .bg-lista{
                background-color: white;
            }

            p{
                font-size: 32px;
                font-weight: lighter !important;
            }
        `;
    }

    constructor() {
        super();
        this.rango = 1;
        this.autos = [];
        this.getAutoData();
    }

    updated(changedProperties){
        if (changedProperties.has("rango")) {
            this.getAutoData();
        }  
    }

    render() {
        return html`
        
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            
            <div class="row bg-lista pb-5">
            <p class="pt-3 px-4">¡Obt&eacute;n el auto de tus sueños!</p>
                <div class="row row-cols-1 row-cols-sm-4 px-4 mt-2">
                ${this.autos.map(
                    auto => html`<app-auto-ficha 
                                        id="${auto.id}" 
                                        marca="${auto.marca}"
                                        modelo="${auto.modelo}"
                                        anno="${auto.año}"
                                        precio="${auto.precio}"
                                        imagen="${auto.imagen}"
                                        @cotizar-auto="${this.cotizarAuto}">
                                </app-auto-ficha>`
                )}
                </div>
            </div>
        `;
    }

    cotizarAuto(e){
        this.dispatchEvent(
            new CustomEvent("cotizar-auto", {
                    bubbles: true,
                    detail: e.detail
                }
            )
        );
    }

    getAutoData(){
        let url="https://cotizador-autos-springboot.herokuapp.com/apicotizador/v1/rangos/"+this.rango+"/autos"
        //let url = "/data/coches"+this.rango+".json"
        let xhr = new XMLHttpRequest();
        xhr.onload = function (){
            if (xhr.status ===200){
                let APIResponse = JSON.parse(xhr.responseText);
                this.autos = APIResponse.autos ;
            }
        }.bind(this);
        xhr.open("GET",url);
        xhr.send();
        
      }
}

customElements.define('app-lista-autos', AppListaAutos)