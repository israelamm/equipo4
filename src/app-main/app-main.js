import {html, LitElement} from 'lit-element'
import '../app-lista-autos/app-lista-autos'
import '../app-form-credito/app-form-credito'
import '../app-cotizador-general/app-cotizador-general'
import '../app-cotizador-plan/app-cotizador-plan'

    class AppMain extends LitElement{
        static get properties() {
            return {
                rangoSelected: { type:Number },
                carSelected: { type: Boolean }
                
            };
        }

        constructor() {
            super();
            this.rangoSelected = 1;
            this.carSelected = {};
        }

        render(){
            return html 
                `
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
                
                <div id="eligeAuto" class="row">
                        <app-form-credito @rank-selected="${this.rankChange}" class="col-12 col-lg-3"></app-form-credito>
                        <app-lista-autos .rango="${this.rangoSelected}" @cotizar-auto="${this.cotizarAuto}" class="col-12 col-lg-9"></app-lista-autos>
                </div>
                <div id="cotizaAuto" class="row d-none bg-white">
                        <app-cotizador-general class="col-12 col-md-5" .auto="${this.carSelected}" @data-to-calculate="${this.calculateData}" @go-to-select-car="${this.showEligeAuto}"></app-cotizador-general>
                        <app-cotizador-plan class="col-12 col-md-7"> </app-cotizador-plan>
                </div>
                `;
        }
        calculateData(e){
            
            console.log("calculate en mmain" + e.detail.cliente);
            this.shadowRoot.querySelector('app-cotizador-plan').dataToCalculate = e.detail;
            this.shadowRoot.querySelector('app-cotizador-plan').consultarCotizacion = true;
        }

        rankChange(e) {
            this.rangoSelected = e.detail;
        }

        cotizarAuto(e){
            console.log(e.detail);
            this.shadowRoot.querySelector('#eligeAuto').classList.add("d-none");
            this.shadowRoot.querySelector("#cotizaAuto").classList.remove("d-none");
            this.carSelected = e.detail;
        }

        showEligeAuto(){
            this.shadowRoot.querySelector('#eligeAuto').classList.remove("d-none");
            this.shadowRoot.querySelector("#cotizaAuto").classList.add("d-none");
        }
    }

customElements.define('app-main', AppMain);