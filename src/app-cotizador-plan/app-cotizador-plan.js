import { LitElement, html, css } from 'lit-element';

class AppCotizadorPlan extends LitElement {

    static get properties() {
        return {
            cotizacion:{type:Object},
            dataToCalculate:{type:Object},
            consultarCotizacion:{type:Boolean}
        };
    }

    static get styles(){
        return css
        `
        tr:hover>th, tr:hover>td{
            background-color: #072146;
            color: #2dcccd !important;
            font-weight: bold;
        }

        th{
            font-size:16px;
            color: #1973b8;
        }

        tr,th,td{
            
            border:none !important;
        }

        td{
                font-size: 14px;
        }

        tbody, tr{
            background-color: #f4f4f4;
        }


        button{
            color: white;
            font-size: 15px;
 
            font-weight: bold;
 
            outline:none;
 
            border:none;
 
            height: 50px;
 
            background-color: #1973b8;
        }
 
        button:focus{
         outline: none;
         box-shadow: none;
       }

       h5{
           font-size: 26px;
           font-weight: bold !important;
           text-transform: uppercase;
       }
        `;
    }

    constructor() {
        super();
        this.cotizacion = {};
        this.cotizacion.capital = 0;
        this.cotizacion.intereses = 0;
        this.cotizacion.total=0;
        this.cotizacion.meses=0;
        this.cotizacion.mensualidad=0;
        this.cotizacion.tasa=0;
        this.dataToCalculate = {};
        this.dataToCalculate.cliente="";
        this.consultarCotizacion = false;
        //this.getCotizacionData();
    }

    updated(){
        console.log("en updated del plan "+this.dataToCalculate.cliente);
        if(this.consultarCotizacion===true){
            this.getCotizacionData();
            this.consultarCotizacion=false;
        }
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <div class="mt-3">
            <center><h5>Cotizaci&oacute;n para el cr&eacute;dito</h6></center>
            <br>
            <table class="table h-100">
                
                <tbody>
                    <tr>
                        <th scope="row">Capital</th>
                        <td>$${this.numberWithCommas(this.cotizacion.capital)}</td>
                    </tr>
                    <tr>
                        <th scope="row">Intereses</th>
                        <td>$${this.numberWithCommas(this.cotizacion.intereses)}</td>
                    </tr>
                    <tr>
                        <th scope="Total">Total</th>
                        <td>$${this.numberWithCommas(this.cotizacion.total)}</td>
                    </tr>
                    <tr>
                        <th scope="row">Meses</th>
                        <td>${this.cotizacion.meses}</td>
                    </tr>
                    <tr>
                        <th scope="row">Mensualidad</th>
                        <td>$${this.numberWithCommas(this.cotizacion.mensualidad)}</td>
                    </tr>
                    <tr>
                        <th scope="row">Tasa</th>
                        <td>${this.cotizacion.tasa}%</td>
                    </tr>
                </tbody>
                </table>

                <button @click="${this.saveCotizacion}" class="col-12 my-3">Guardar cotizaci&oacute;n</button>
            <div>
        `;
    }

    getCotizacionData(){
        console.log ("en cotizacion plan"+this.dataToCalculate.id)

        let url = "https://cotizador-autos-springboot.herokuapp.com/apicotizador/v1/cotizacion?"+
        "id="+this.dataToCalculate.id+
        "&enganche="+this.dataToCalculate.enganche+
        "&meses="+this.dataToCalculate.meses;

        console.log("la url: "+url);

        //let url = "/data/cotizar.json"
        let xhr = new XMLHttpRequest();
        xhr.onload = function (){
            if (xhr.status ===200){
                console.log("peticion correcta");
                console.log("respuesta: "+xhr.responseText)
                let APIResponse = JSON.parse(xhr.responseText);
                console.log("respuesta: ")
                this.cotizacion = APIResponse ;
    
                console.log("al final"+this.cotizacion)
            }
        }.bind(this);
        xhr.open("GET",url);
        xhr.send();
        
    }

    saveCotizacion(){
        console.log("en guardar cotizacion");

        let url = "https://cotizador-autos-springboot.herokuapp.com/apicotizador/v1/cotizacion";
        console.log("el nombre: "+this.dataToCalculate.nombre);
        console.log("el ennganche "+this.dataToCalculate.enganche)
        let json = {
            "id":this.dataToCalculate.id,
            "nombre": this.dataToCalculate.cliente,
            "precio": this.cotizacion.precio,
            "enganche": this.dataToCalculate.enganche,
            "capital": this.cotizacion.capital,
            "intereses": this.cotizacion.intereses,
            "total": this.cotizacion.total,
            "meses": this.cotizacion.meses,
            "mensualidad": this.cotizacion.mensualidad,
            "tasa": this.cotizacion.tasa
        };

        console.log("json a guardar: "+JSON.stringify(json));

        console.log("json :"+json);
        let xhr = new XMLHttpRequest();
        xhr.onload = function (){
            if (xhr.status ===200){
                console.log("peticion correcta");
                console.log("respuesta: "+xhr.responseText)
                alert("Cotización creada exitosamente");
            }
        }.bind(this);
        xhr.open("POST",url);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(json));

    }

     

      numberWithCommas(x) {
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1,$2");
        return x;
    }
 
}

customElements.define('app-cotizador-plan', AppCotizadorPlan)