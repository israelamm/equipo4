import {html, LitElement, css} from 'lit-element'
import './app-header/app-header.js'
import './app-main/app-main.js'
import './app-busca-cotizacion/app-busca-cotizacion'
import './app-footer/app-footer.js'

    class BaseLitelement extends LitElement{

        static get styles(){
            return css
            `
                div{
                    background-color: #f4f4f4;
                }
            `;
        }

        render(){
            return html 
                `
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

                    <app-header @go-to-main="${this.showMain}" @go-to-quotes="${this.showQuotes}"></app-header>
                    <div class="container-fluid">
                        <app-main></app-main>
                        <app-busca-cotizacion class="d-none"></app-busca-cotizacion>
                    </div>
                    <app-footer></app-foter>
                `;
        }

        showMain() {
            this.shadowRoot.querySelector('app-main').classList.remove("d-none");
            this.shadowRoot.querySelector("app-busca-cotizacion").classList.add("d-none");
        }

        showQuotes() {
            this.shadowRoot.querySelector('app-main').classList.add("d-none");
            this.shadowRoot.querySelector("app-busca-cotizacion").classList.remove("d-none");
        }
    }

customElements.define('base-litelement', BaseLitelement);
