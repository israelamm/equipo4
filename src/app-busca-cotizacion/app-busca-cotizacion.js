import { LitElement, html, css } from 'lit-element';

export class AppBuscaCotizacion extends LitElement {
    static get properties() {
      return {
        cotizaciones: { type: Array },
        flag: { type: Boolean }
      };
    }
  
    static get styles() {
      return css`
      button{
        color: white;
        font-size: 15px;

        font-weight: bold;

        outline:none;

        border:none;


        background-color: #1973b8;
    }

    .card{
      border: none;
    }

    button:focus{
     outline: none;
     box-shadow: none;
   }

   .form-control{
     height: 60px !important;
   }

   h5{
    color: #121212;
    font-size: 26px !important;
    text-transform: uppercase;
    font-weight: bold;
   }

   .div-th{
     font-size: 16px;
     background-color: #f4f4f4;
     height: 50px;
     font-weight:bold;
   }

   .div-tr{
    font-size: 13px;
     background-color: #f4f4f4;
     height: 40px;
     text-transform: uppercase;
   }

   .div-error-message{
     background-color: #072146 !important;
     color: white;
     font-size: 15px;

   }

   .card-footer{
     background-color: white;
     border: none;
   }

   .div-seguros{
    background-color: #eaf9fa;
    font-size: 18px;
   }

   .text-min{
    font-size: 13px;
   }

   .label-btn{
    height: 40px !important;
    line-height: 40px;
   }
      `;
    }
  
    constructor() {
      super();
      this.cotizaciones = [];
      this.flag = false;
    }
    

    updated(changedProperties) { 
      if (changedProperties.has("cotizaciones")) {
        if (this.cotizaciones.length > 0) {
          this.shadowRoot.querySelector('#errorMessage').classList.add("d-none");
          this.shadowRoot.querySelector('.container').classList.remove("d-none");
          this.shadowRoot.querySelector('#btn-busqueda').classList.add("d-none");
          this.shadowRoot.querySelector('#btn-nueva-busqueda').classList.remove("d-none");
        } else if (this.flag) {
          this.shadowRoot.querySelector('#errorMessage').classList.remove("d-none");
        }
        this.flag = true;
      }
    }
  
    render() {
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <div class="card h-100 p-0">
          <div class="card-body">
              <h5 class="card-title">Ingresa el nombre de quien solicitó la cotización</h5>
              <div class="container-fluid mt-3 mb-4">
                <div class="row mt-2">
                  <div class="col-12 col-md-6 col-xl-4 p-0">
                    <div class="form-floating">
                      <input type="text" class="form-control" id="txtNomCot" placeholder="Buscar ...">
                      <label>Buscar</label>
                    </div>
                  </div>
                  <div id="btn-busqueda" class="col-12 col-md-4 col-lg-2 mt-2 mt-md-0">
                    <button @click="${this.searchQuote}" class="col-12 h-100"><label class="label-btn">Buscar</label></button>
                  </div>
                  <div id="btn-nueva-busqueda" class="col-12 col-md-4 col-lg-2 d-none mt-2 mt-md-0">
                    <button @click="${this.newSearch}" class="col-12 h-100"><label class="label-btn">Nueva Búsqueda</label></button>
                  </div>
                </div>
              </div>


              <div id="errorMessage" class="row justify-content-center my-5 d-none px-3 px-md-0 text-center">
                <div class="col-12 col-md-6 col-xl-4 my-1 div-error-message">
                  <span>
                  <img src="https://www.bbva.mx/content/dam/public-web/global/images/micro-illustrations/error.svg"  height="60" width="60" class="img-fluid"/>

                  No existen cotizaciones del cliente   ingresado.
                  </span>
                  
                </div>
                </div>
              
              <div class="container mt-5 mb-3 div-tabla d-none">
                  <div class="row div-th align-items-center">
                        <div class="col d-none d-lg-inline">Marca</div>
                        <div class="col">Modelo</div>
                        <div class="col d-none d-lg-inline">Año</div>
                        <div class="col d-none d-lg-inline">Precio</div>
                        <div class="col d-none d-lg-inline">Enganche</div>
                        <div class="col">Meses</div>
                        <div class="col d-none d-lg-inline">Capital</div>
                        <div class="col d-none d-lg-inline">Intereses</div>
                        <div class="col">Mensualidad</div>
                        <div class="col d-none d-lg-inline">Tasa de interes</div> 
                  </div>

                  
              ${this.cotizaciones.map(
                cotizacion => html`
                  <div class="row pt-2 div-tr">
                    <div class="col d-none d-lg-inline">${cotizacion.marca}</div>
                    <div class="col">${cotizacion.modelo}</div>
                    <div class="col d-none d-lg-inline">${cotizacion.anno}</div>
                    <div class="col d-none d-lg-inline">${cotizacion.precio}</div>
                    <div class="col d-none d-lg-inline">${cotizacion.enganche}</div>
                    <div class="col">${cotizacion.meses}</div>
                    <div class="col d-none d-lg-inline">${cotizacion.capital}</div>
                    <div class="col d-none d-lg-inline">${cotizacion.intereses}</div>
                    <div class="col">${cotizacion.mensualidad}</div>
                    <div class="col d-none d-lg-inline">${cotizacion.tasa}</div>
                  </div>`
                )}
              </div>
          </div>
          <div class="card-footer px-3 px-md-0">

                <div class="container div-seguros mt-3 mb-5 w-100">
                  <div class="row p-0 w-100">
                    <div class="col-12 col-md-3 text-center">
                      <img src="https://www.bbva.mx/content/dam/public-web/global/images/micro-illustrations/car_insurance.svg"  height="100" width="120" class="img-fluid"/>

                    </div>

                    <div class="col-12 col-md-9 text-center py-4">
                    <label>
                      Contrata un seguro para tu auto y recibe cashback o meses sin intereses al pagar con tarjetas de crédito BBVA.
                      </label>
                      <p class="text-min mt-3">Al momento de la contratación tu asesor te informará los beneficios y los términos y condiciones.</p>
                    </div>
                  </div>
                </div>
            
          </div>
        </div> 
      `;
    }

    getCotizacionData(name){
      console.log (name)
      //let url = "/data/resCotizaciones.json"
      let url = "https://cotizador-autos-springboot.herokuapp.com/apicotizador/v1/cotizaciones?nombre=" + name;
      let xhr = new XMLHttpRequest();
      xhr.onload = function (){
          if (xhr.status ===200){
              let APIResponse = JSON.parse(xhr.responseText);
              this.cotizaciones = APIResponse;
          }
      }.bind(this);
      xhr.open("GET", url);
      xhr.send();
    }

    searchQuote() {
      this.cotizaciones = [];
      this.getCotizacionData(this.shadowRoot.querySelector('#txtNomCot').value);
    }

    newSearch() {
      this.shadowRoot.querySelector('.container').classList.add("d-none");
      this.shadowRoot.querySelector('#errorMessage').classList.add("d-none");
      this.shadowRoot.querySelector('#btn-nueva-busqueda').classList.add("d-none");
      this.shadowRoot.querySelector('#btn-busqueda').classList.remove("d-none");
      this.shadowRoot.querySelector('#txtNomCot').value = '';
    }
}

customElements.define('app-busca-cotizacion', AppBuscaCotizacion);


